<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title>listado de usuarios - Styde.net</title>
    </head>
    <body>
<h1> {{e($title)}})</h1>
   <ul>
       @foreach ($users as $user)
           <li>{{e($user)}}</li>
       @endforeach
   </ul>
    </body>
</html>
