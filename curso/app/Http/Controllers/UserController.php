<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
    $users = [
        'joel','Ellie','Tess','Tommy'
];

        return view('users',[
            'users'=> $users,
            'title' => 'listado de usuarios'
        ]);
    }

    public function show($id)
    {
        return "mostrando detalles del usuario {$id}";
    }

    public function create(){
        return  'crea usuario nuevo';
    }
}
