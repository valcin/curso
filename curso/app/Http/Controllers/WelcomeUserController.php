<?php

namespace App\Http\Controllers;


class WelcomeUserController extends Controller
{

   public function name($name, $nickname =null) {
       if ($nickname == null) {
           return "beinvenido {$name}";
       }
       return "bienvenido {$name}, tu apodo es {$nickname}";
   }
}



