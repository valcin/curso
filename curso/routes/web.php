<?php

route::get ('/' ,function ()
{
    return 'home';
});

route::get ('/usuarios' , 'UserController@index');

route::get('usuarios/nuevos', 'UserController@create');

route::get('/usuarios/{id}', 'UserController@show')
     ->where('id', '[0-9]+');

route::get('/saludo/{name}/{nickname?}', 'WelcomeUserController@name');


